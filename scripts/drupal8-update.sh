#!/bin/bash

##
# XLD Drupal 8 Update Script.
#
# This is a script to update Drupal 8 via composer & run update.php.
##

DRUSH_DIR=$(drush dd @drupal.local)
REPO_DIR=$(cd $DRUSH_DIR../ && pwd)

echo -e "\nSetting working directory to $REPO_DIR ...\n"
cd $REPO_DIR
echo -e "\nRunning 'composer update' ...\n"
composer update
echo -e "\nSetting working directory to $DRUSH_DIR ...\n"
cd $DRUSH_DIR
echo -e "\nRunning update.php ...\n"
drush updb -y
echo -e "\nAll done.\n"
