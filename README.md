# Dockerized Drupal Project

A configurable Docker and Composer-enabled Drupal development environment.

This project provides a Docker-based framework for creating & configuring a
Drupal site.
    - Drupal 8 development should follow the Composer-based approach outlined in
      the GitHub project [Composer template for Drupal projects][].
    - Drupal 7 development may follow either a Composer-based approach or a
      non-Composer 'traditional' approach.

[Composer template for Drupal projects]: https://github.com/drupal-composer/drupal-project

## Local Development with Docker

The Docker portion of this repository consists of three main components:

1. This `README.md` file.
2. A `.env` file, used for setting project-specific environment variables.
2. A `docker-compose.yml` file, used for defining Docker containers and for
   calling additional Docker provisioning scripts.
3. A `docker` directory, which contains the additional Docker scripts as well as
   web engine connfiguration files.

### Official containers only

All of this is based on officials containers, so you won't download any
specific containers to use this project.

#### What's included

- Apache2/Nginx
- MySQL/Mariadb
- PHP
- Drush
- MailHog
- Adminer
- Redis (disabled by default)
- Solr (disabled by default)

### Requirements for local development

- [Composer](https://getcomposer.org/download/)
- [Docker](https://docs.docker.com/engine/installation/)
    - Windows users: Be sure open Docker settings after installing and share
      your local drive!
- [Docker Compose](https://docs.docker.com/engine/installation/)

### First-time Project config

1. Clone this repo to your local, open a terminal, and `cd` into the project.
   Remove all contents of the `html` directory, cd into the `html` directory,
   and initialize a new Git repo for your project.

        git clone https://bitbucket.org/exeldigital/dockerized-drupal-project.git myproject
        cd myproject

2. Install web and php dependency files so that the docroot is located in the
   `html/web` directory.

        # Drupal 8:
        rm -r html
        # composer create-project drupal-composer/drupal-project:8.x-dev tmp --no-interaction
        composer create-project drupal/recommended-project html
        cd html
        composer require drush/drush
        composer require drupal/admin_toolbar
        composer require drupal/devel

        # Drupal 7:
        rm -rf html
        composer require drush/drush
        vendor/bin/drush dl drupal-7 --drupal-project-rename=web

3. Open up `.env` and set environment variables for the project.
    - Give the project a name (local domain name will be {PROJECT}.dev)
        - Replace all occurences of `drupal.dev` in this `README.md` file with
          `{PROJECT}.dev`.
    - Set the last two sets of digits of the IP address to random numbers
      (between 2 & 254).
        - Replace all occurrences of `127.0.100.100` in this `README.md` file
          with the updated IP.

4. Commit all changes to your new Git repo. Add a remote repo and push.

        git add ./
        git commit -m 'Initial commit: Dockerized Drupal Project'
        git remote add origin git@{mygitservice}:{myaccount}/{myproject}.git
        git push -u origin master

### Get started

1. Add the project domain to your local Hosts file, and point it to
   `127.0.100.100`.
    - Paste the following into `/etc/hosts`
      (`c:\Windows\System32\drivers\etc\hosts` for Windows users):

            127.0.100.100  drupal.dev db.drupal.dev solr.drupal.dev

    - Mac users: Run the following to attach an unused IP to the lo0 interface.

            sudo ifconfig lo0 alias 127.0.100.100/24

2. Clone this repository to your local, open a terminal, and `cd` into the
   project.

3. Start the local web server:

        docker-compose up -d

#### Import your database or install your site.

Installing a new Drupal site with the Standard install profile:

```sh
cd web
../vendor/bin/drush site-install standard --db-url=mysql://drupal:drupal@db.drupal.dev:3306/drupal --account-pass=admin
../vendor/bin/drush uli --uri=drupal.dev
```

Installing Drupal with settings defined in /config/sync:

```sh
cd web
../vendor/bin/drush site-install minimal --db-url=mysql://drupal:drupal@db.drupal.dev:3306/drupal --account-pass=admin
../vendor/bin/drush site-install config_installer --account-pass=admin
../vendor/bin/drush uli --uri=drupal.dev
```

Installing Drupal using an existing database:

Place a gzipped copy of your project's database dump file at:
`/docker/db/drupal.sql.gz`

- The first time you run `docker-compose up`, Docker Compose will automatically
  import any `.sql` or `.sql.gz` files that are placed in `/docker/db/ref`. The
  server will create each database to match the dump file's base file name.

```sh
cd html
vendor/bin/drush @drupal.test sql-dump --structure-tables-list=cache,cache_* | gzip > ../docker/db/ref/drupal.sql.gz
vendor/bin/drush rsync @drupal.test:%files @drupal.local:%files
cd ..
docker-compose down
docker-compose up -d
cd html
vendor/bin/drush updb
vendor/bin/drush cr
vendor/bin/drush uli
```

### Database credentials

Use this is for local development only!

* User: drupal
* Password: drupal
* Database: drupal
* Host: db.drupal.dev
* Port: 3306

### Links to local development environment & tools

- [Drupal Site](http://drupal.dev)
- [phpMyAdmin](http://drupal.dev:8181)
- [MailHog](http://drupal.dev:8025)
- [Solr Admin](http://drupal.dev:8983) (disabled by default)

### Other useful shell commands

#### Opening a Bash terminal into the 'php' Docker container

Open a terminal into an already-running container:

```sh
docker-compose exec php bash
drush status
```

Spin up a container and enter it:

```sh
# cd into the project root.
docker-compose run --rm php bash
drush status
```

- Windows users: This is unavailable, for now. Docker Compose for Windows
  currently does not support [interactive mode][].

[interactive mode]: https://github.com/docker/compose/issues/3194

#### Using Docker Compose in the project directory

```sh
docker-compose build  # Build or rebuild services.
docker-compose up     # Create and start containers (foreground).
docker-compose up -d  # Create and start containers (background).
docker-compose ps     # List containers.
docker-compose stop   # Stop services.
# Use with caution - this will blow away your project's database:
docker-compose down   # Remove containers, networks, images, and volumes.
```

#### Checking up on Docker containers

```sh
docker ps                   # List running containers.
docker ps -a                # List all containers.
docker images               # List all images.
docker stop $(docker ps -q) # Stop all running containers
```

#### Cleaning up
```sh
# Updated clean-up commands!
docker system prune
docker network prune

# Delete all images (USE WITH CAUTION):
docker rmi $(docker images -a -q)
```

### Debugging PHP with Sublime Text

This still needs testing.

1. Install [Package Control][].
2. Install the [Xdebug Client][] package using Package Control.
3. Navigate to Tools -> Xdebug -> Settings-User
4. Paste in the following, replacing `local/project/path` with your project path:

        {
          "path_mapping":
          {
            "/var/www/html/web": "local/project/path/web"
          },
          "follow_symlinks": true,
          "super_globals": true,
          "close_on_stop": true,
          "max_children": 32,
          "max_data": 1024,
          "max_depth": 4
        }

5. Open `docker-compose.yml`, and make sure the `remote_host` value under the
   `php` section matches the IP of your local host.
    - Linux users: Try using value `172.17.0.1`.
    - Windows users: Try using value `10.33.36.1`.
    - Be sure to restart the php container after changing this value.
6. Add breakpoints and enable your Xdebug client using
   "fn + command + shift + F9" or go to Tools -> Xdebug -> Start Debugging, then
   navigate to your site and refresh it.

[Package Control]: https://packagecontrol.io/installation
[Xdebug Client]: https://packagecontrol.io/packages/Xdebug%20Client

### Switching between Apache/Nginx/MariaDB/MySQL/PHP services

#### Using Apache2

1. Open `docker-compose.yml` and make the following changes:
    - Comment out the `nginx` section.
    - Uncomment the `${IP_ADDRESS}:80:80` line under the `php` section.
2. Open `docker/php/Dockerfile`, and make the following changes:
    - Change line 1 to: `FROM php:7-apache`
    - Change the last line to: `CMD ["apache2-foreground"]`
3. Re-build the php container:

        docker-compose build

#### Using Nginx (default)

1. Open `docker-compose.yml` and make the following changes:
    - Uncomment the `nginx` section.
    - Comment out the `${IP_ADDRESS}:80:80` line under the `php` section.
2. Open `docker/php/Dockerfile`, and make the following changes:
    - Change line 1 to: `FROM php:7-fpm`
    - Change the last line to: `CMD ["php-fpm"]`

#### Selecting a PHP version

1. Open `docker/php/Dockerfile`, and update the tag in line 1 with your desired
   version.
    - `FROM php:xxx-fpm` (Nginx) or `FROM php:xxx-apache` where `xxx` is the
      appropriate version/tag.
    - Available php tags: https://hub.docker.com/r/library/php/tags/
2. re-build the php container:

        docker-compose build

#### Selecting a database service & version

1. Open `docker-compose.yml` and find the `image` declaration under the `db`
   section.
2. Assign an image & tag with your desired service (e.g. `image: mysql:5.5`).
    - Available mariadb tags: https://hub.docker.com/r/library/mariadb/tags/
    - Available mysql tags: https://hub.docker.com/r/library/mysql/tags/

#### Enabling Redis

1. Open `docker-compose.yml` and make the following changes:
    - Uncomment the `redis` line  under the `php` section.
    - Uncomment the `redis` section near line 53.
2. Open `docker/php/Dockerfile`, and uncomment the `Install Redis support`
   code section near line 33.

#### Enabling Solr

1. Open `docker-compose.yml` and make the following changes:
    - Uncomment the `solr` line under the `php` section.
    - Uncomment the `solr` section near line 55.

#### Installing self-signed certificate on host machine

1. Download latest pre-built binary of mkcert from
   [mkcert releases page](https://github.com/FiloSottile/mkcert/releases).
2. Rename file to `mkcert` and place in `/usr/local/bin/mkcert`.
3. Run: `mkcert-install`. No need to use sudo.
4. Create cert pair for project site: `mkcert drupal.dev`
5. Rename files `ssl.crt` and `ssl.key`
6. Place ssl files in `/certs`

## Todo

- Test on Mac.
- Think about build scripts for maintaining support client sites.

## References & Thanks

- [Docker4Drupal | GitHub][1]
- [Composer template for Drupal projects | GitHub][2]
- [Enabling MailHog and mhsendmail in Docker | Random musings of a Geek][3]
- [Debug your PHP in Docker with Intellij/PHPStorm and Xdebug | GitHub Gist][4]
- [Compose file reference | Docker][5]
- [Improving dev environments: All the HTTP things | Adrian Perez][6]
- [Docker Compose Zero-Downtime Deployment | GitHub][7]
- [no servers are inside upstream in `/etc/nginx/conf.d/default.conf:34` | GitHub][8]
- [No docker0 on docker for mac? | GitHub][9]
- [Docker Compose Local HTTPS with nginx or Caddy and mkcert | Code with Hugo][10]

[1]: https://github.com/Wodby/docker4drupal
[2]: https://github.com/drupal-composer/drupal-project
[3]: https://blog.cod3r.co.uk/?p=64
[4]: https://gist.github.com/chadrien/c90927ec2d160ffea9c4
[5]: https://docs.docker.com/compose/compose-file
[6]: https://adrianperez.org/improving-dev-environments-all-the-http-things
[7]: https://github.com/vincetse/docker-compose-zero-downtime-deployment
[8]: https://github.com/jwilder/docker-gen/issues/196
[9]: https://github.com/docker/docker/issues/22753#issuecomment-246054946
[10]: https://codewithhugo.com/docker-compose-local-https/
