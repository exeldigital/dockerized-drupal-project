Place your mysql dump files here to be automatically imported by mysql.

Allowable names:

- `drupal.sql`
- `drupal.sql.gz`

For more information: <https://hub.docker.com/r/library/mysql/>
